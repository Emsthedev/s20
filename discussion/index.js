console.log("Hello");

console.log("");

/* 

        While loop - takes a single
        condition. If the condition is true 
        it will run the code.

        Syntax:

        while(condition){
            statment
        }

*/

let count=5;

while (count !== 0) {
    console.log("While: " + count);

    count--;
}
console.log("");
let num = 0;

while (num <= 5) {

    
     console.log("While: " + num);
     num++;
}

//Mini Activity While
console.log("Mini Activity :");
let numA = 0;

while (numA <= 30) {
    console.log("Mini Activity: " + numA);
    numA+=2;
}

/* 
        Do While Loop
        -a do while loop works a lot like while loop
        But inlike loops, do-while guarantee that 
        the code will be executed atleast once.


        Syntax:
                    do {
                        //statements
                    } while (express/condition) {
                        //statements
                    }

*/

/* let number = Number(prompt("Give me a number:"));

do {
    console.log("Do while :" + number);
    number++;
} while (number < 10) {
    //statements
} */

// e


/* 
    For loop 
        - more flexible that while loop and do-while

        -part: 



        Syntax:

        for (initialValue; condition; iteration){
                    statement
        }
*/


for (let countt = 0; countt <= 20; countt+=2) {
    
    console.log("For loop :" + countt);
}

let myString = "emman garcia"
console.log(myString.length);


console.log("");

console.log(myString[0]);
console.log(myString[9]);

console.log("");

for(let x =0; x < myString.length; x++){
    console.log(myString[x]);
}

console.log("");

let myName = " JOSEPHINE";

for (let n = 0; n < myName.length; n++) {
   
    if (
        
        
        myName[n].toLowerCase() == "a" ||
        myName[n].toLowerCase() == "e" ||
        myName[n].toLowerCase() == "i" ||
        myName[n].toLowerCase() == "o" ||
        myName[n].toLowerCase() == "u"  ) 

        {
            console.log("Vowel")
        } else{
            console.log( myName[n])
        }
        
    }


    //Mini Activity

    console.log("");
    let myWord = " extravagant";
    let myCons= "";

for (let w = 0; w < myWord.length; w++) {
   
    if (
        
        
        myWord[w].toLowerCase() == "a" ||
        myWord[w].toLowerCase() == "e" ||
        myWord[w].toLowerCase() == "i" ||
        myWord[w].toLowerCase() == "o" ||
        myWord[w].toLowerCase() == "u"  ) 

        {
            console.log("Vowel")
            continue;
        } else{
             myCons += myWord[w];
            
        }
        
    }
    console.log(myCons);


    // Continue & Break
/* 
            Continue statements - allows the code to
            go to the next iteration without finishing the next iteration 
            without finishing the execution of all the
*/
    console.log("");

    for (let s = 0; s <= 20; s++) {
       
        if(s % 5 === 0){
            console.log("Div by 5")
            continue;
        }
        console.log("continue and break " + s)
        if (s > 10){
            break;
        }
    }


    console.log(" ");

    let name2 = "Alexander";

    for (let i = 0; i < name2.length; i++){
        console.log(name2[i])

        if (name2[i].toLowerCase() === "a"){
            console.log("continue to the iteration.")
            continue;

        }

        
        if (name2[i].toLowerCase() === "r"){
            console.log("continue to the iteration.")
            break;

        }
    }