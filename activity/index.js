// S 20 - Javascript Repetition Control Structures

// ACTIVITY


/*
PART 1: 

	- Create a variable n that will store the value of the n provided by the user via the prompt.

	- Create a for loop that will be initialized with the n provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

	- Create a condition that if the current value is less than or equal to 50, stop the loop.

	- Create another condition that if the current value is divisible by 10, print a message that the n is being skipped and continue to the neit iteration of the loop.

	- Create another condition that if the current value is divisible by 5, print the n.

*/

// CODE HERE:
let n = parseInt(prompt("Enter Your number:"));
console.log("The number you provided is " + n);
console.log("");

for (let i = n; i >= 0; i--) {


    if (i <= 50) {
        console.log(
            "The current value is less than or equal to 50. Terminating the loop.");
        break;
    }

    if (i % 10 === 0) {
        console.log("The number is divisible by 10. Skipping the number");
        continue;
    }

    if (i % 5 === 0) {
        console.log(i);
        continue;
    }




}




/*
PART 2:

	- Create a varaible that will contain the string "supercalifragilisticeipialidocious"

	- Create another varaible that will store the consonants from the string.

	- Create a for loop that will iterate through the individual letters of the string based on it's length.

	- Create an if statement that will check if the individual letters of the string is equal to a vowel and continue to the neit iteration of the loop if it is true.

	- Create an else statement that will add the consonants to the second variable.


*/

// CODE HERE:








console.log("");
let myWord = " supercalifragilisticeipialidocious";
let myCons = "";

for (let w = 0; w < myWord.length; w++) {

    if (


        myWord[w].toLowerCase() == "a" ||
        myWord[w].toLowerCase() == "e" ||
        myWord[w].toLowerCase() == "i" ||
        myWord[w].toLowerCase() == "o" ||
        myWord[w].toLowerCase() == "u")

    {
        console.log(myWord)
        continue;
    } else {
        myCons += myWord[w];

    }

}
console.log(myCons);